Stylefree  — интернет-магазин сумок и шопперов. 
Сайт является зачётным проектом IT-школы Bonch.dev 2019. 
За неделю были реализованы основные страницы сайта и обеспечено взаимодействие с сервером. Проект написан на Vue/Quasar.
Сертификат об успешном прохождении школы по направлению Front-end: https://drive.google.com/file/d/1j5SIubSUemV8E3stpxSPNwGEuEa8fRda/view?usp=sharing

# Stylefree (final)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
